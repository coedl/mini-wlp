# Warlpiri Dictionary Project (mini version)

## About

This repository contains the data validation and conversion scripts for preparing the Warlpiri dictionary database for print publication.
With the exception of the dictionary database (only a 'mini' version is included in this public-facing repository), the data processing scripts are pretty much identical to those used in our private repository with the full database.

## The lexical database

The lexical database is provided in `src/wlp-lexicon_master.txt`. The database uses an SGML-like markup language where tags are prefixed with a backslash code, e.g. `\me` to indicate the start of a **m**ain **e**ntry.

```
\me jampaly(pa) (N, PV): (La,Wi,Y)
\gl sharp, pointed \egl
\rv sharp \erv
\eg
\we Karlangu ka karri jampalypa ngulaju yiri-nyayirni. \[@@]
\et The digging stick is sharp, that is very sharp pointed. \ewe
\eeg
\ant jampilypa, munju \eant
\syn larrilpi, yiri \esyn
\eme
```

## Parser

The parser is written as a Nearley EBNF grammar (https://nearley.js.org/), and can be found in `scripts/parser/wlp-skeleton.ne`. The grammar specifies which tag sequences are licit, e.g. an `\me` line *must* be followed by a `\gl` line, etc.

## Test suite

The data is passed through a series of tests implemented as an R package: https://coedl.github.io/yinarlingi/. The tests run automatically as a GitLab pipeline (see `.gitlab-ci.yml`) and uploaded to GitLab pages:

<img width="1268" alt="Screenshot 2023-01-31 at 1 36 16 PM" src="https://user-images.githubusercontent.com/9938298/215888879-55ffee49-09fb-47ea-b3be-15e31daf237b.png">

## Converted outputs

### PDF

A typeset PDF preview is also uploaded to GitLab pages and can be viewed on: https://coedl.gitlab.io/mini-wlp/wlp-lexicon.pdf

<img width="1165" alt="Screenshot 2023-01-31 at 1 42 01 PM" src="https://user-images.githubusercontent.com/9938298/215890334-b1b1a564-9864-4dec-8206-457094f14d0c.png">

### InDesign-compliant XML

The dictionary database in XML for import into InDesign is available on: https://coedl.gitlab.io/mini-wlp/wlp-xml.zip